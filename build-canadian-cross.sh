#!/bin/bash
##############################################################################
# Copyright (c) 2023 Marcus Geelnard
#
# This software is provided 'as-is', without any express or implied warranty.
# In no event will the authors be held liable for any damages arising from the
# use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
#  1. The origin of this software must not be misrepresented; you must not
#     claim that you wrote the original software. If you use this software in
#     a product, an acknowledgment in the product documentation would be
#     appreciated but is not required.
#
#  2. Altered source versions must be plainly marked as such, and must not be
#     misrepresented as being the original software.
#
#  3. This notice may not be removed or altered from any source distribution.
##############################################################################

err_report() {
    echo "*** Failed to build: Stopped on line $1"
    if [ -n "${LATEST_LOG}" ] ; then
        echo ""
        echo "Latest log:"
        cat "${LATEST_LOG}"
    fi
}
trap 'err_report ${LINENO}' ERR
set -e

function help {
    echo "Usage: $0 [options] [component]"
    echo ""
    echo "Build and install the MRISC32 GNU toolchain."
    echo ""
    echo "Options:"
    echo "  --target=TARGET  Build for TARGET (default: mrisc32-elf)"
    echo "  --host=HOST      Cross-compile for HOST (e.g. x86_64-w64-mingw32)"
    echo "                     If specified, a \"Canadian cross\" build will be"
    echo "                     performed. If not specified, a native build will"
    echo "                     be performed."
    echo "  --prefix=PATH    Set installation path (default: ${HOME}/.local)"
    echo "  -c, --clean      Clean the build directories before building"
    echo "  -u, --update     Update the Git submodules"
    echo "  -jN              Use N parallel processes (note: no space)"
    echo "  -h, --help       Show this text"
    echo ""
    echo "Component:"
    echo "  all              Build all components (default)"
    echo "  binutils         Build only binutils"
    echo "  bootstrap        Build only the bootstrap version of GCC"
    echo "                     (requires binutils)"
    echo "  newlib           Build only newlib"
    echo "                     (requires bootstrap)"
    echo "  gcc              Build only GCC"
    echo "                     (requires newlib)"
}

# Parse arguments.
DO_CLEAN=no
DO_UPDATE=no
PREFIX=${HOME}/.local
HOST=""
TARGET=mrisc32-elf
BUILD_BINUTILS=yes
BUILD_BOOTSTRAP=yes
BUILD_NEWLIB=yes
BUILD_GCC=yes
NUM_PROCESSES=""
for arg in "$@" ; do
    case ${arg} in
        -h|--help)
            help
            exit 0
            ;;
        -c|--clean)
            DO_CLEAN=yes
            ;;
        -u|--update)
            DO_UPDATE=yes
            ;;
        -j*)
            NUM_PROCESSES="${arg:2}"
            ;;
        --prefix=*)
            PREFIX="${arg#*=}"
            ;;
        --host=*)
            HOST="${arg#*=}"
            ;;
        --target=*)
            TARGET="${arg#*=}"
            ;;
        all)
            BUILD_BINUTILS=yes
            BUILD_BOOTSTRAP=yes
            BUILD_NEWLIB=yes
            BUILD_GCC=yes
            ;;
        binutils)
            BUILD_BINUTILS=yes
            BUILD_BOOTSTRAP=no
            BUILD_NEWLIB=no
            BUILD_GCC=no
            ;;
        bootstrap)
            BUILD_BINUTILS=no
            BUILD_BOOTSTRAP=yes
            BUILD_NEWLIB=no
            BUILD_GCC=no
            ;;
        newlib)
            BUILD_BINUTILS=no
            BUILD_BOOTSTRAP=no
            BUILD_NEWLIB=yes
            BUILD_GCC=no
            ;;
        gcc)
            BUILD_BINUTILS=no
            BUILD_BOOTSTRAP=no
            BUILD_NEWLIB=no
            BUILD_GCC=yes
            ;;
        *)
            echo "*** Invalid argument: ${arg}"
            help
            exit 1
            ;;
    esac
done

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "${SCRIPT_DIR}"

# Determine the BUILD and HOST platforms (e.g. x86_64-pc-linux-gnu).
BUILD=$(ext/gcc-mrisc32/config.guess)
if [ -z "${HOST}" ] ; then
    HOST=${BUILD}
fi
if [ "${BUILD}" = "${HOST}" ] ; then
    CANADIAN_CROSS=no
    echo "Building a native ${TARGET} cross compiler."
else
    CANADIAN_CROSS=yes
    echo "Building a ${TARGET} cross compiler for ${HOST} hosts (Canadian cross)."
fi

# Set up compiler for the build platform.
BUILD_CC=${BUILD_CC:-""}
BUILD_CXX=${BUILD_CXX:-""}
BUILD_CFLAGS=${BUILD_CFLAGS:-"-O2"}
BUILD_CXXLAGS=${BUILD_CXXLAGS:-"-O2"}
BUILD_LDFLAGS=${BUILD_LDFLAGS:-""}

# Set up compiler for the host platform.
if [ "${CANADIAN_CROSS}" == "yes" ] ; then
    HOST_CC=${HOST_CC:-""}
    HOST_CXX=${HOST_CXX:-""}
    HOST_CFLAGS=${HOST_CFLAGS:-"-O2"}
    HOST_CXXLAGS=${HOST_CXXLAGS:-"-O2"}
    HOST_LDFLAGS=${HOST_LDFLAGS:-""}
else
    HOST_CC=${BUILD_CC}
    HOST_CXX=${BUILD_CXX}
    HOST_CFLAGS=${BUILD_CFLAGS}
    HOST_CXXLAGS=${BUILD_CXXLAGS}
    HOST_LDFLAGS=${BUILD_LDFLAGS}
fi

activate_build_compiler() {
    export CC=${BUILD_CC}
    export CXX=${BUILD_CXX}
    export CFLAGS=${BUILD_CFLAGS}
    export CXXFLAGS=${BUILD_CXXFLAGS}
    export LDFLAGS=${BUILD_LDFLAGS}
}

activate_host_compiler() {
    export CC=${HOST_CC}
    export CXX=${HOST_CXX}
    export CFLAGS=${HOST_CFLAGS}
    export CXXFLAGS=${HOST_CXXFLAGS}
    export LDFLAGS=${HOST_LDFLAGS}
}

# Determine number of parallel processes.
if [ -z "${NUM_PROCESSES}" ] ; then
    NUM_PROCESSES=$(getconf _NPROCESSORS_ONLN 2>/dev/null || echo 8)
    if [ -z "${NUM_PROCESSES}" ]; then
        NUM_PROCESSES = 8
    fi
    ((NUM_PROCESSES=$NUM_PROCESSES+1))
fi
echo "INFO: Using ${NUM_PROCESSES} parallel processes where possible"
echo ""

# Update the git submodules.
if [ "${DO_UPDATE}" == "yes" ] ; then
    git submodule update --init --recursive
fi

# Make sure that we have output folders.
OUT=out
BUILD_OUT=out-build
mkdir -p "${OUT}"
mkdir -p "${BUILD_OUT}"

# Create the install roots (${PREFIX} and ${BUILD_PREFIX}), and make sure that
# ${BUILD_PREFIX} is the first in our PATH during installation.
BUILD_PREFIX=${SCRIPT_DIR}/${BUILD_OUT}/install
mkdir -p "${PREFIX}"
mkdir -p "${BUILD_PREFIX}"
PATH="${BUILD_PREFIX}/bin:${PATH}"

#
# This is inspired by http://www.ifp.illinois.edu/~nakazato/tips/xgcc.html
#
# Also see: https://gist.github.com/knotdevel/c57086222c83fc8b26bcf94d1aed66eb
#

# Build binutils.
if [ "${BUILD_BINUTILS}" == "yes" ] ; then
    if [ "${CANADIAN_CROSS}" == "yes" ] ; then
        # ---------------------------------------------
        # For the BUILD platform.
        # ---------------------------------------------
        echo "==[ binutils - ${BUILD} -> ${TARGET} ]=="
        activate_build_compiler
        if [ "${DO_CLEAN}" == "yes" ] ; then
            echo "  Cleaning..."
            rm -rf ${BUILD_OUT}/binutils
        fi
        mkdir -p ${BUILD_OUT}/binutils

        echo "  Configuring..."
        cd ${BUILD_OUT}/binutils
        LATEST_LOG=${PWD}/configure.log
        ../../ext/binutils-mrisc32/configure \
            --prefix="${BUILD_PREFIX}" \
            --build="${BUILD}" \
            --host="${BUILD}" \
            --target="${TARGET}" \
            --disable-gdb \
            --disable-nls \
            --disable-sim \
            --without-zstd \
            > "${LATEST_LOG}" 2>&1

        echo "  Building..."
        LATEST_LOG=${PWD}/build.log
        MAKE_FLAGS=""
        if [ "${DO_CLEAN}" == "yes" ] ; then
            # We only allow parallel make for clean builds, as it seems to cause problems for
            # incremental builds.
            MAKE_FLAGS=-j${NUM_PROCESSES}
        fi
        make ${MAKE_FLAGS} all > "${LATEST_LOG}" 2>&1

        echo "  Installing..."
        LATEST_LOG=${PWD}/install.log
        make install > "${LATEST_LOG}" 2>&1
        cd ../..
        echo ""
    fi

    # ---------------------------------------------
    # For the HOST platform.
    # ---------------------------------------------
    echo "==[ binutils - ${BUILD} -> ${HOST} -> ${TARGET} ]=="
    activate_host_compiler
    if [ "${DO_CLEAN}" == "yes" ] ; then
        echo "  Cleaning..."
        rm -rf ${OUT}/binutils
    fi
    mkdir -p ${OUT}/binutils

    echo "  Configuring..."
    cd ${OUT}/binutils
    LATEST_LOG=${PWD}/configure.log
    ../../ext/binutils-mrisc32/configure \
        --prefix="${PREFIX}" \
        --build="${BUILD}" \
        --host="${HOST}" \
        --target="${TARGET}" \
        --disable-gdb \
        --disable-sim \
        --without-zstd \
        > "${LATEST_LOG}" 2>&1

    echo "  Building..."
    LATEST_LOG=${PWD}/build.log
    MAKE_FLAGS=""
    if [ "${DO_CLEAN}" == "yes" ] ; then
        # We only allow parallel make for clean builds, as it seems to cause problems for
        # incremental builds.
        MAKE_FLAGS=-j${NUM_PROCESSES}
    fi
    make ${MAKE_FLAGS} all > "${LATEST_LOG}" 2>&1

    echo "  Installing..."
    LATEST_LOG=${PWD}/install.log
    make install > "${LATEST_LOG}" 2>&1
    cd ../..
    echo ""
fi

# Build bootstrap gcc.
if [ "${BUILD_BOOTSTRAP}" == "yes" ] ; then
    # ---------------------------------------------
    # For the BUILD platform.
    # ---------------------------------------------
    echo "==[ Bootstrap GCC - ${BUILD} -> ${TARGET} ]=="
    activate_build_compiler
    if [ "${DO_CLEAN}" == "yes" ] ; then
        echo "  Cleaning..."
        rm -rf ${BUILD_OUT}/gcc-bootstrap
    fi
    mkdir -p ${BUILD_OUT}/gcc-bootstrap
    echo "  Downloading prerequisites..."
    cd ext/gcc-mrisc32
    LATEST_LOG=../../${BUILD_OUT}/gcc-bootstrap/prerequisites.log
    ./contrib/download_prerequisites > "${LATEST_LOG}" 2>&1
    cd ../..

    echo "  Configuring..."
    cd ${BUILD_OUT}/gcc-bootstrap
    LATEST_LOG=${PWD}/configure.log
    ../../ext/gcc-mrisc32/configure \
        --prefix="${BUILD_PREFIX}" \
        --build="${BUILD}" \
        --host="${BUILD}" \
        --target="${TARGET}" \
        --enable-languages=c,c++ \
        --disable-nls \
        --without-headers \
        --with-newlib \
        --with-gnu-as \
        --with-gnu-ld \
        --without-zstd \
        > "${LATEST_LOG}" 2>&1

    echo "  Building..."
    LATEST_LOG=${PWD}/build.log
    make -j"${NUM_PROCESSES}" all-gcc all-target-libgcc > "${LATEST_LOG}" 2>&1

    echo "  Installing..."
    LATEST_LOG=${PWD}/install.log
    make install-gcc install-target-libgcc > "${LATEST_LOG}" 2>&1
    cd ../..
    echo ""
fi

# Build newlib.
if [ "${BUILD_NEWLIB}" == "yes" ] ; then
    if [ "${CANADIAN_CROSS}" == "yes" ] ; then
        # ---------------------------------------------
        # For the BUILD platform.
        # ---------------------------------------------
        echo "==[ newlib - ${BUILD} -> ${TARGET} ]=="
        activate_build_compiler
        if [ "${DO_CLEAN}" == "yes" ] ; then
            echo "  Cleaning..."
            rm -rf ${BUILD_OUT}/newlib
        fi
        mkdir -p ${BUILD_OUT}/newlib

        echo "  Configuring..."
        cd ${BUILD_OUT}/newlib
        LATEST_LOG=${PWD}/configure.log
        ../../ext/newlib-mrisc32/configure \
            --prefix="${BUILD_PREFIX}" \
            --build="${BUILD}" \
            --host="${BUILD}" \
            --target="${TARGET}" \
            > "${LATEST_LOG}" 2>&1

        echo "  Building..."
        LATEST_LOG=${PWD}/build.log
        make -j"${NUM_PROCESSES}" all > "${LATEST_LOG}" 2>&1

        echo "  Installing..."
        LATEST_LOG=${PWD}/install.log
        make install > "${LATEST_LOG}" 2>&1
        cd ../..
        echo ""
    fi

    # ---------------------------------------------
    # For the HOST platform.
    # TODO: We should be able to use the build
    # from the BUILD platform for the HOST platform
    # too by simply copying/installing the relevant
    # files, instead of building newlib twice.
    # ---------------------------------------------
    echo "==[ newlib - ${BUILD} -> ${HOST} -> ${TARGET} ]=="
    activate_host_compiler
    if [ "${DO_CLEAN}" == "yes" ] ; then
        echo "  Cleaning..."
        rm -rf ${OUT}/newlib
    fi
    mkdir -p ${OUT}/newlib

    echo "  Configuring..."
    cd ${OUT}/newlib
    LATEST_LOG=${PWD}/configure.log
    ../../ext/newlib-mrisc32/configure \
        --prefix="${PREFIX}" \
        --build="${BUILD}" \
        --host="${HOST}" \
        --target="${TARGET}" \
        > "${LATEST_LOG}" 2>&1

    echo "  Building..."
    LATEST_LOG=${PWD}/build.log
    make -j"${NUM_PROCESSES}" all > "${LATEST_LOG}" 2>&1

    echo "  Installing..."
    LATEST_LOG=${PWD}/install.log
    make install > "${LATEST_LOG}" 2>&1

    cd ../..
    echo ""
fi

# Build gcc with newlib.
if [ "${BUILD_GCC}" == "yes" ] ; then
    # ---------------------------------------------
    # For the HOST platform.
    # ---------------------------------------------
    echo "==[ GCC - ${BUILD} -> ${HOST} -> ${TARGET} ]=="
    activate_host_compiler
    if [ "${DO_CLEAN}" == "yes" ] ; then
        echo "  Cleaning..."
        rm -rf ${OUT}/gcc
    fi
    mkdir -p ${OUT}/gcc
    echo "  Downloading prerequisites..."
    cd ext/gcc-mrisc32
    LATEST_LOG=../../${OUT}/gcc/prerequisites.log
    ./contrib/download_prerequisites > "${LATEST_LOG}" 2>&1
    cd ../..

    echo "  Configuring..."
    cd ${OUT}/gcc
    LATEST_LOG=${PWD}/configure.log
    ../../ext/gcc-mrisc32/configure \
        --prefix="${PREFIX}" \
        --build="${BUILD}" \
        --host="${HOST}" \
        --target="${TARGET}" \
        --enable-languages=c,c++ \
        --with-newlib \
        --with-gnu-as \
        --with-gnu-ld \
        --disable-shared \
        --disable-libssp \
        --disable-libstdcxx-pch \
        --without-zstd \
        > "${LATEST_LOG}" 2>&1

    echo "  Building..."
    LATEST_LOG=${PWD}/build.log
    make -j"${NUM_PROCESSES}" all > "${LATEST_LOG}" 2>&1

    echo "  Installing..."
    LATEST_LOG=${PWD}/install.log
    make install > "${LATEST_LOG}" 2>&1

    cd ../..
    echo ""
fi

# Strip binaries.
echo "Stripping binaries..."
find "${PREFIX}/bin" -executable -type f -exec strip {} \; 2>/dev/null || true
find "${PREFIX}/libexec/gcc" -name 'cc1*' -executable -type f -exec strip {} \; 2>/dev/null || true
find "${PREFIX}/libexec/gcc" -name 'lto-wrapper*' -executable -type f -exec strip {} \; 2>/dev/null || true
find "${PREFIX}/libexec/gcc" -name 'collect2*' -executable -type f -exec strip {} \; 2>/dev/null || true
find "${PREFIX}/libexec/gcc" -name 'd21*' -executable -type f -exec strip {} \; 2>/dev/null || true
find "${PREFIX}/libexec/gcc" -name 'lto1*' -executable -type f -exec strip {} \; 2>/dev/null || true
find "${PREFIX}/${TARGET}/bin" -executable -type f -exec strip {} \; 2>/dev/null || true
echo ""

echo "Build and installation finished succesfully!"

