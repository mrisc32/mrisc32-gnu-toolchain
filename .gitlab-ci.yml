---
workflow:
  name: "Build MRISC32 GNU toolchain"
  rules:
    - if: $CI_COMMIT_TAG && $CI_COMMIT_TAG =~ /^v[0-9]+.*/

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/releases/${CI_COMMIT_TAG}"
  PACKAGE_FILE_LINUX_X86_64: "mrisc32-gnu-toolchain-linux-x86_64.tar.gz"
  PACKAGE_FILE_LINUX_ARM64: "mrisc32-gnu-toolchain-linux-arm64.tar.gz"
  PACKAGE_FILE_LINUX_ARMV7: "mrisc32-gnu-toolchain-linux-armv7.tar.gz"
  PACKAGE_FILE_WIN_X86_64: "mrisc32-gnu-toolchain-win-x86_64.zip"
  PACKAGE_FILE_MACOS: "mrisc32-gnu-toolchain-macos.zip"

stages:
  - build
  - upload
  - release

build_job_linux_x86_64:
  stage: build
  image: registry.gitlab.com/mrisc32/mrisc32-gnu-toolchain/builder:20.04-2
  variables:
    CC: "gcc"
    CXX: "g++"
    CFLAGS: "-O2"
    CXXFLAGS: "-O2"
  script:
    - |
      # Build
      mkdir -p mrisc32-gnu-toolchain
      ./build.sh --prefix=${PWD}/mrisc32-gnu-toolchain --clean
    - |
      # Add GPL license files.
      cp ext/binutils-mrisc32/COPYING* mrisc32-gnu-toolchain/
      cp ext/newlib-mrisc32/COPYING* mrisc32-gnu-toolchain/
      cp ext/gcc-mrisc32/COPYING* mrisc32-gnu-toolchain/
    - |
      # Package the archive.
      tar -cv -I "gzip -9" -f ${PACKAGE_FILE_LINUX_X86_64} mrisc32-gnu-toolchain

  artifacts:
    expose_as: 'MRISC32 GNU Toolchain Linux x86_64'
    name: 'mrisc32-gnu-toolchain-linux-x86_64'
    paths:
      - ${PACKAGE_FILE_LINUX_X86_64}

build_job_linux_arm64:
  stage: build
  image: registry.gitlab.com/mrisc32/mrisc32-gnu-toolchain/builder:20.04-2
  variables:
    HOST_CC: aarch64-linux-gnu-gcc
    HOST_CXX: aarch64-linux-gnu-g++
  script:
    - |
      # Build
      mkdir -p mrisc32-gnu-toolchain
      ./build-canadian-cross.sh --host=aarch64-linux-gnu --prefix=${PWD}/mrisc32-gnu-toolchain --clean
    - |
      # Add GPL license files.
      cp ext/binutils-mrisc32/COPYING* mrisc32-gnu-toolchain/
      cp ext/newlib-mrisc32/COPYING* mrisc32-gnu-toolchain/
      cp ext/gcc-mrisc32/COPYING* mrisc32-gnu-toolchain/
    - |
      # Package the archive.
      tar -cv -I "gzip -9" -f ${PACKAGE_FILE_LINUX_ARM64} mrisc32-gnu-toolchain

  artifacts:
    expose_as: 'MRISC32 GNU Toolchain Linux ARM64'
    name: 'mrisc32-gnu-toolchain-linux-arm64'
    paths:
      - ${PACKAGE_FILE_LINUX_ARM64}

build_job_linux_armv7:
  stage: build
  image: registry.gitlab.com/mrisc32/mrisc32-gnu-toolchain/builder:20.04-2
  variables:
    HOST_CC: arm-linux-gnueabihf-gcc
    HOST_CXX: arm-linux-gnueabihf-g++
  script:
    - |
      # Build
      mkdir -p mrisc32-gnu-toolchain
      ./build-canadian-cross.sh --host=arm-linux-gnueabihf --prefix=${PWD}/mrisc32-gnu-toolchain --clean
    - |
      # Add GPL license files.
      cp ext/binutils-mrisc32/COPYING* mrisc32-gnu-toolchain/
      cp ext/newlib-mrisc32/COPYING* mrisc32-gnu-toolchain/
      cp ext/gcc-mrisc32/COPYING* mrisc32-gnu-toolchain/
    - |
      # Package the archive.
      tar -cv -I "gzip -9" -f ${PACKAGE_FILE_LINUX_ARMV7} mrisc32-gnu-toolchain

  artifacts:
    expose_as: 'MRISC32 GNU Toolchain Linux ARMv7'
    name: 'mrisc32-gnu-toolchain-linux-armv7'
    paths:
      - ${PACKAGE_FILE_LINUX_ARMV7}

build_job_win_x86_64:
  stage: build
  image: registry.gitlab.com/mrisc32/mrisc32-gnu-toolchain/builder:20.04-2
  variables:
    HOST_CC: x86_64-w64-mingw32-gcc
    HOST_CXX: x86_64-w64-mingw32-g++
    HOST_LDFLAGS: "-static -static-libgcc -static-libstdc++"
  script:
    - |
      # Build
      mkdir -p mrisc32-gnu-toolchain
      ./build-canadian-cross.sh --host=x86_64-w64-mingw32 --prefix=${PWD}/mrisc32-gnu-toolchain --clean
    - |
      # Add GPL license files.
      cp ext/binutils-mrisc32/COPYING* mrisc32-gnu-toolchain/
      cp ext/newlib-mrisc32/COPYING* mrisc32-gnu-toolchain/
      cp ext/gcc-mrisc32/COPYING* mrisc32-gnu-toolchain/
    - |
      # Package the archive.
      7z a -tzip -mx=9 ${PACKAGE_FILE_WIN_X86_64} mrisc32-gnu-toolchain

  artifacts:
    expose_as: 'MRISC32 GNU Toolchain Win x86_64'
    name: 'mrisc32-gnu-toolchain-win-x86_64'
    paths:
      - ${PACKAGE_FILE_WIN_X86_64}

build_job_macos:
  stage: build
  tags:
    - saas-macos-medium-m1
  image: macos-15-xcode-16
  variables:
    HOMEBREW_NO_AUTO_UPDATE: 1
    CC: "clang"
    CXX: "clang++"
    CFLAGS: "-O2 -arch x86_64 -arch arm64"
    CXXFLAGS: "-O2 -arch x86_64 -arch arm64"
  before_script:
    # Install necessary packages.
    - brew install texinfo
  script:
    # Override the antiquated system makeinfo on macOS with the one installed by brew.
    # This solves build errors in the binutils/bfd documentation (2023-02-07).
    - export PATH="/opt/homebrew/opt/texinfo/bin:$PATH"

    - |
      # Build
      mkdir -p mrisc32-gnu-toolchain
      ./build.sh --prefix=${PWD}/mrisc32-gnu-toolchain --clean
    - |
      # Add GPL license files.
      cp ext/binutils-mrisc32/COPYING* mrisc32-gnu-toolchain/
      cp ext/newlib-mrisc32/COPYING* mrisc32-gnu-toolchain/
      cp ext/gcc-mrisc32/COPYING* mrisc32-gnu-toolchain/
    - |
      # Package the archive.
      zip -r9 ${PACKAGE_FILE_MACOS} mrisc32-gnu-toolchain

  artifacts:
    expose_as: 'MRISC32 GNU Toolchain macOS'
    name: 'mrisc32-gnu-toolchain-macos'
    paths:
      # For some reason ${PACKAGE_FILE_MACOS} does not work here. Is it a bug?
      - mrisc32-gnu-toolchain-macos.zip

upload_job:
  stage: upload
  image: curlimages/curl:latest
  needs:
    - job: build_job_linux_x86_64
      artifacts: true
    - job: build_job_linux_armv7
      artifacts: true
    - job: build_job_linux_arm64
      artifacts: true
    - job: build_job_win_x86_64
      artifacts: true
    - job: build_job_macos
      artifacts: true
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${PACKAGE_FILE_LINUX_X86_64}" "${PACKAGE_REGISTRY_URL}/${PACKAGE_FILE_LINUX_X86_64}"
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${PACKAGE_FILE_LINUX_ARM64}" "${PACKAGE_REGISTRY_URL}/${PACKAGE_FILE_LINUX_ARM64}"
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${PACKAGE_FILE_LINUX_ARMV7}" "${PACKAGE_REGISTRY_URL}/${PACKAGE_FILE_LINUX_ARMV7}"
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${PACKAGE_FILE_WIN_X86_64}" "${PACKAGE_REGISTRY_URL}/${PACKAGE_FILE_WIN_X86_64}"
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${PACKAGE_FILE_MACOS}" "${PACKAGE_REGISTRY_URL}/${PACKAGE_FILE_MACOS}"

release_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: upload_job
  script:
    - echo 'Running release_job'
  release:
    tag_name: '$CI_COMMIT_TAG'
    name: 'Release $CI_COMMIT_TAG'
    description: 'Release for tag $CI_COMMIT_TAG'
    assets:
      links:
        - name: "${PACKAGE_FILE_LINUX_X86_64}"
          filepath: "/${PACKAGE_FILE_LINUX_X86_64}"
          url: "${PACKAGE_REGISTRY_URL}/${PACKAGE_FILE_LINUX_X86_64}"
        - name: "${PACKAGE_FILE_LINUX_ARM64}"
          filepath: "/${PACKAGE_FILE_LINUX_ARM64}"
          url: "${PACKAGE_REGISTRY_URL}/${PACKAGE_FILE_LINUX_ARM64}"
        - name: "${PACKAGE_FILE_LINUX_ARMV7}"
          filepath: "/${PACKAGE_FILE_LINUX_ARMV7}"
          url: "${PACKAGE_REGISTRY_URL}/${PACKAGE_FILE_LINUX_ARMV7}"
        - name: "${PACKAGE_FILE_WIN_X86_64}"
          filepath: "/${PACKAGE_FILE_WIN_X86_64}"
          url: "${PACKAGE_REGISTRY_URL}/${PACKAGE_FILE_WIN_X86_64}"
        - name: "${PACKAGE_FILE_MACOS}"
          filepath: "/${PACKAGE_FILE_MACOS}"
          url: "${PACKAGE_REGISTRY_URL}/${PACKAGE_FILE_MACOS}"

